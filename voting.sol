pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/ownership/Ownable.sol"

/** 
 * @title Voting
 */
contract Voting {

    struct Voter {
      bool isRegistered;
      bool hasVoted;
      uint votedProposalId;
    }
    
    struct Proposal {
      string description;
      uint voteCount;
    }

    struct Vote {
      address voter;
      uint proposalId;
    }
    
    Vote[] public allVotes;
    
    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }
    
    event VoterRegistered(address indexed voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address indexed voter, uint proposalId);

    WorkflowStatus public workflowStatus;
    uint public winningProposalId;

    mapping(address => Voter) public voters;
    Proposal[] public proposals;

    constructor() {
        workflowStatus = WorkflowStatus.RegisteringVoters;
    }

    modifier atStage(WorkflowStatus _stage) {
        require(workflowStatus == _stage, "Erreur : Etat incorrect");
        _;
    }

    modifier isRegisteredVoter() {
        require(voters[msg.sender].isRegistered, "Erreur : Vous n'êtes pas enregistré");
        _;
    }

    modifier hasNotVoted() {
        require(!voters[msg.sender].hasVoted, "Erreur : Vous avez déjà voté");
        _;
    }

    modifier isOwnerOrAtStage(WorkflowStatus _stage) {
        require(owner() == msg.sender || workflowStatus == _stage, "Erreur : Etat incorrect");
        _;
    }

    function startProposalsRegistration() external onlyOwner atStage(WorkflowStatus.RegisteringVoters) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, WorkflowStatus.ProposalsRegistrationStarted);
    }

    function endProposalsRegistration() external onlyOwner atStage(WorkflowStatus.ProposalsRegistrationStarted) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, WorkflowStatus.ProposalsRegistrationEnded);
    }

    function startVotingSession() external onlyOwner atStage(WorkflowStatus.ProposalsRegistrationEnded) {
        workflowStatus = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, WorkflowStatus.VotingSessionStarted);
    }

    function endVotingSession() external onlyOwner atStage(WorkflowStatus.VotingSessionStarted) {
        workflowStatus = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, WorkflowStatus.VotingSessionEnded);
    }

    function tallyVotes() external onlyOwner atStage(WorkflowStatus.VotingSessionEnded) {
        workflowStatus = WorkflowStatus.VotesTallied;
        uint winningVoteCount = 0;

        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningVoteCount) {
                winningVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }

        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, WorkflowStatus.VotesTallied);
    }

    function registerVoter(address _voterAddress) external onlyOwner atStage(WorkflowStatus.RegisteringVoters) {
        require(!voters[_voterAddress].isRegistered, "Erreur : Vote déjà enregistré");
        voters[_voterAddress].isRegistered = true;
        emit VoterRegistered(_voterAddress);
    }

    function submitProposal(string memory _description) external isRegisteredVoter atStage(WorkflowStatus.ProposalsRegistrationStarted) {
        proposals.push(Proposal({
            description: _description,
            voteCount: 0
        }));
        emit ProposalRegistered(proposals.length - 1);
    }

    function vote(uint _proposalId) external isRegisteredVoter hasNotVoted atStage(WorkflowStatus.VotingSessionStarted) {
      require(_proposalId < proposals.length, "Erreur : ID incorrect");
      
      Vote memory vote = Vote({
          voter: msg.sender,
          proposalId: _proposalId
      });
      allVotes.push(vote);

      voters[msg.sender].hasVoted = true;
      voters[msg.sender].votedProposalId = _proposalId;
      proposals[_proposalId].voteCount++;
      
      emit Voted(msg.sender, _proposalId);
    }

    function getWinner() public view atStage(WorkflowStatus.VotesTallied) returns (uint) {
        require(winningProposalId < proposals.length, "Erreur : Aucun gagnant n'a encore été déterminé");
        return winningProposalId;
    }

    function getAllVotes() external view returns (Vote[] memory) {
        return allVotes;
    }

}
